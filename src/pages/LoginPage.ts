import "webdriverio";

class LoginPage{
      
  get elements() {
    return {
      usernameInput: $('#user_login'),
      passwordInput: $('#user_pass'),
      loginBtn: $('#wp-submit'),
      message : $("#msg")
    }
  }  

  navigateTo() : void{
    browser.url('/login');
    browser.waitUntil(async ()=>{
      return (await this.elements.loginBtn).isDisplayed();
    })
  }

  public async login(username : string, password : string){
    await (await this.elements.usernameInput).setValue(username);
    await (await this.elements.passwordInput).setValue(password);
    await (await this.elements.loginBtn).click();
  }


}

export default new LoginPage();