import chaiHttp = require('chai-http');
import chai = require('chai');
import {expect} from 'chai';
import {Response} from 'superagent'

chai.use(chaiHttp);

const BASE_URL : string = "https://www.boredapi.com";

describe("API Test suite", () =>{
    it("Get to /api/activity/ should has price property", (done : MochaDone) => {
        chai.request(BASE_URL)
        .get("/api/activity/")
        .end((err : any, res : Response ) => {
            expect(err).to.be.null;
            expect(res).to.be.json;
            expect(res).to.have.status(200);
            expect(res.body).to.haveOwnProperty('price');
            done();
        });    
    });
    it("Get to api/activity?type=recreational should has property type equal to recreational", (done : MochaDone) => {
        chai.request(BASE_URL)
        .get("/api/activity?type=recreational")
        .end((err : any, res : Response ) => {
            expect(err).to.be.null;
            expect(res).to.be.json;
            expect(res).to.have.status(200);            
            expect(res.body).to.have.property('type').equal('recreational');
            done();
        });    
    });
    it("Get to /api/activity?key=5881028 shouldn't have property participants more than 1", (done : MochaDone) => {
        chai.request(BASE_URL)
        .get("/api/activity?key=5881028")
        .end((err : any, res : Response ) => {
            expect(err).to.be.null;
            expect(res).to.be.json;
            expect(res).to.have.status(200);            
            expect(res.body).to.have.property('participants').lte(1);
            done();
        });    
    });
});