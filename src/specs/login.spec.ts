import LoginPage  from '../pages/LoginPage';
import {assert} from 'chai';

const username = 'newuser4';
const password = 'newpass4';

describe('Login functionality test', () => {
    it('Should display Student not associated to class', async () => {
        const msg = "Student not associated to class";
        LoginPage.navigateTo();
        await LoginPage.login(username, password);
        assert.equal(await (await LoginPage.elements.message).getText(), msg)
    });
});