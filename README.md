#Test project for Beable.
##The content of the project:
- LoginPage.ts - TypeScript source file containing the Page Object model for login process
- login.spec.ts - TypeScript source file containing the test case code for UI Automation test
- api.spec.ts - TypeScript source file containing the test cases code for API Automation test

## Setup
How to start from scratch.
- Install NodeJS >=10.15.1
- Install TypeScript >=3.6.4.
- Get node modules with `npm install`

## How to use
To run all tests type `npm test`